<?php

class Challengue2Solution
{
    public function post_confirm(Service $servicio)
    {
        $serviceId = Input::get('service_id');
        $driverId = Input::get('driver_id');

        if ($servicio->status_id == Servicio::NO_DISPONIBLE) {
            return Response::json(json(array('error' =>  Servicio::NO_DISPONIBLE)));
        }

        if ($servicio->driver_id == NULL && $servicio->status_id == Servicio::DISPONIBLE) {
            $driver = Driver::find($driverId);
            $driver->update(['available' => Driver::NO_DISPONIBLE]);

            $servicio->update($serviceId, array(
                'driver_id' => $driverId,
                'car_id' => $driver->car_id,
                'status_id' => Servicio::EN_PROCESO
            ));

            // Delegate service notification to another class, userType will help to decide if the notification
            // goes to android or ios.
            $servicio->notify($servicio->user->type, $servicio, 1, 'honk.wav', 'Open', array('serviceId' => $servicio->id));

            return Response::json(array('error' => 'NO_ERROR'));
        }
    }
}
