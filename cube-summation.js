function getValuesFromOperation(operation) {
    const values = operation.split(' ').map(Number);
    values.shift();
    return values;
}

function isInTheRangeOfQuery(update, values) {
    const [x, y, z, value] = update;
    const [x1, y1, z1, x2, y2, z2] = values;

    return x1 <= x && x <= x2
        && y1 <= y && y <= y2
        && z1 <= z && z <= z2;
}

function queryUpdates (updates, values) {
    const sum = updates.reduce((sum, update) => {
        const [x, y, z, value] = update;

        return isInTheRangeOfQuery(update, values) ? sum + value : sum;
    }, 0);

    console.log(sum);
}

function processData(input) {
    const data = input.split('\n');
    const numberOfTests = data.shift();

    for (let i = 0; i < numberOfTests; i++) {
        const structure = data.shift().split(' ');
        const operationsQuantity = parseInt(structure[1]);
        const operations = data.splice(0, operationsQuantity);

        const updates = [];

        operations.forEach(operation => {
            const values = getValuesFromOperation(operation);
            if (operation.includes('QUERY')) {
                queryUpdates(updates, values);
            } else {
                updates.push(values);
            }
        });
    }
} 
